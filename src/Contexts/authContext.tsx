import React, { useState, createContext } from "react";
import { checkUserData, IUserIdentity, key } from "../Pages/Login/users";
import { ChildrenProps } from "./deleteContext";

export type AuthType = {
    userData: IUserIdentity,
    setUserData: Function
}

const AuthContext = createContext<AuthType | null>(null);

export const AuthProvider: React.FC<ChildrenProps> = ({ children }) => {
    debugger

    const [userData, setUserData] = useState(() => {
        return checkUserData();
    });

    return (
        <AuthContext.Provider value={{ userData, setUserData }} >
            {children}
        </AuthContext.Provider>
    );

}

export default AuthContext;