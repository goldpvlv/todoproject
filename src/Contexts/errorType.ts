export type ErrorType = {
    showError: boolean;
    setError: Function;
}