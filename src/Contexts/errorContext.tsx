import React, { useState, createContext } from "react";
import { ErrorType } from "./errorType";

export interface ChildrenProps {
    children: React.ReactNode;
}

export const ErrorContext = createContext<ErrorType | null>(null);

export const ErrorContextProvider: React.FC<ChildrenProps> = ({ children }) => {
    const [showError, setError] = useState(false);

    return (
        <ErrorContext.Provider value={{ showError, setError }}>
            {children}
        </ErrorContext.Provider>
    );

}