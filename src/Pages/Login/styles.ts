
import styled from 'styled-components';

export const Img = styled.img`
    width: 30vw;
`

export const Page = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    flex: row;
`
export const LoginLayout = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction:column;
    justify-content:center;
    align-items: left;
    padding-left:12.5vw ;
    box-sizing: border-box;
`

export const Title = styled.h1`
    color: #333;
    font-family: Roboto, Arial;
    font-size:40px;
    margin:12px;
    margin-left:0;
`

export const Subtitle = styled.h2`
    color: #e06666;
    font-family: Roboto, Arial;
    font-size:16px;
    font-weight: ;
    margin:8px;
    margin-left:0;
`

export const FieldName = styled.h2`
    color: #777;
    font-family: Roboto, Arial;
    font-size:16px;
    font-weight: ;
    margin:8px;
    margin-left:0;
`

export const InputField = styled.input`
    width: 25vw;
    height: 40px;
    border-radius: 8px;
    border: 1px solid #999;
    padding-left:8px;
    font-size: 16px;
    color: #777;
    box-sizing: border-box;


    &:focus{
        border: 1px solid #999;
        outline:none;
    }

    &::placeholder{
        color: #bbb;

    }

    `

export const SignIn = styled.button`
        width: 25vw;
        height: 40px;
        border-radius: 8px;
        background: #555;
        color: white;
        font-size:16px;
        border: 0px;
        font-weight:400;

        margin: 16px 0px;

        &:hover{
            background: #454545;
            cursor:pointer;
        }
        
    `