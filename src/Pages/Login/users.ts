export interface IUserIdentity {
    username: string;
    password: string;
}

export const key: string = 'cat';

export const user: IUserIdentity = {
    username: "user",
    password: "12345"
}

export const checkUserData = () => {
    const CryptoJS = require("crypto-js");
    const username = localStorage.getItem('@Project:username');
    const password = localStorage.getItem('@Project:password');
    debugger
    if (!username || !password) {
        localStorage.removeItem('@Project:password');
        localStorage.removeItem('@Project:username');
        return { username: "", password: "" } as IUserIdentity;
    }

    if (username === user.username
        && CryptoJS.AES.decrypt(password, key).toString(CryptoJS.enc.Utf8) === user.password) {
        return { username: username, password: password } as IUserIdentity
    } else {
        localStorage.removeItem('@Project:username');
        localStorage.removeItem('@Project:password');
        return { username: "", password: "" } as IUserIdentity;
    }
}