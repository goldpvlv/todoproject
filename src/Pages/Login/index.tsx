import React, { useState, useContext } from "react";
import * as S from "./styles";
import { Link } from "react-router-dom";
import AuthContext, { AuthType } from "../../Contexts/authContext";
import CryptoJS from "crypto-js";
import { checkUserData, IUserIdentity, key } from "./users";
import { ErrorContext } from "../../Contexts/errorContext";
import { ErrorType } from "../../Contexts/errorType";

const Login: React.FC = () => {
    const { setUserData } = useContext(AuthContext) as AuthType;
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const { showError } = useContext(ErrorContext) as ErrorType;
    const { setError } = useContext(ErrorContext) as ErrorType;

    function handleLogin() {
        localStorage.setItem('@Project:username', username);
        localStorage.setItem('@Project:password', String(CryptoJS.AES.encrypt(password, key)));
        const validateUser: IUserIdentity = checkUserData();
        debugger
        if (!validateUser.password && !validateUser.username ){
            setError(true);
        }
        setUserData(validateUser);
    }

    function handleUserName(event: React.ChangeEvent<HTMLInputElement>) {
        setUserName(event.target.value);
    }

    function handlePassword(event: React.ChangeEvent<HTMLInputElement>) {
        setPassword(event.target.value);
    }

    return (
        <S.Page>
            <S.LoginLayout>
                <S.Title>My simple ToDoList (^_~)</S.Title>
                <S.FieldName >Username</S.FieldName>
                {showError && <S.Subtitle>Invalid username or password</S.Subtitle>}
                <S.InputField
                    value={username}
                    id="login"
                    onChange={handleUserName}
                    placeholder="Insert your username"
                />
                <S.FieldName>Password</S.FieldName>
                <S.InputField
                    value={password}
                    id="password"
                    onChange={handlePassword}
                    placeholder="Insert your password"
                    type="password"
                />
                <Link to="/">
                    <S.SignIn onClick={handleLogin}>Sign In</S.SignIn>
                </Link>
            </S.LoginLayout>
        </S.Page>
    )
};

export default Login;

