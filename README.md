# ToDoProject

This is my simple To Do List App. This project implements only the Front-End. But I try to imitated Back-End work by authorization. 
Using the ReactJS library, and also Styled Components replacing pure CSS.

## Getting started

1. Install Node on your machine, via https://nodejs.org/en/.
2. Install Yarn on your machine via terminal command: `sudo npm install -g yarn`
3. Install Git on your machine, via https://git-scm.com/downloads.
4. Clone this repository to your machine.
5. Run ```yarn install``` on the terminal, inside the folder where you downloaded the project, to install all used dependencies.
6. Run `yarn start` to run the project on your browser.
